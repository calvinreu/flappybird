extern game_dynamic game;
extern visual game_graphic;

const Uint32 frame_time = 15;
const size_t MovePerFrame = 2;

#include <iostream>

void gameloop()
{

    bool running = true;
    Uint32 timer;

    SDL_Event event;

	while(running)
	{
        timer = SDL_GetTicks();

		game.move_pillars();

		running = game.colition();

        game.fall();

        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    running = false;
                    break;
                
                case SDL_TEXTINPUT:
                    if (event.text.text[0] == " "[0]){
                        game.jump();
                    }
                break;

            }
        }

        game_graphic.new_frame();

        timer = SDL_GetTicks() - timer;

        if(timer < frame_time)
            SDL_Delay(frame_time - timer);

	}

}
