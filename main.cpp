#include <vector>
#include <string>
#include "window.hpp"
#include "random.hpp"
#include "game_dynamic.hpp"
#include "gameloop.cpp"
#include "game_start.cpp"
#include "init.cpp"


game_dynamic game;
visual game_graphic;



int main(int argc, char const *argv[])
{

    init();
    game_start();
    gameloop();
    
    return 0;
}