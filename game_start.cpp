extern visual game_graphic;
extern game_dynamic game;
extern const Uint32 frame_time;
extern const size_t pillar_rin;
extern const size_t MovePerFrame;
extern const size_t pillar_width;
extern const size_t screenwidth;

const size_t frames_before_game = 120;

void game_start()
{

    bool running = true;
    Uint32 timer;
    game.flappybird.x_fall = 0;

    int top_pillar_height = (random_int() % 410) + 130;
    SDL_Rect topdrect = {.x = screenwidth, .y = 0, .w = pillar_width, .h = top_pillar_height};
    SDL_Rect topsrcrect = {.x = 0, .y = 0, .w = pillar_width, .h = top_pillar_height};
    SDL_Rect botdrect = {.x = screenwidth, .y = top_pillar_height + 300, .w = pillar_width, .h = 1000 - (top_pillar_height + 300)};
    SDL_Rect botsrcrect = {.x = 0, .y = 0, .w = pillar_width, .h = 1000 - (top_pillar_height + 300)};

    game.pillars->ID_bottom_pillar = game_graphic.add_object(pillar_rin, botsrcrect, botdrect);
    game.pillars->ID_top_pillar    = game_graphic.add_object(pillar_rin, topsrcrect, topdrect);

	for(size_t i = 0; i < frames_before_game && running; i++)
	{
        SDL_Event event;

        timer = SDL_GetTicks();
		
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    running = false;
                    break;
                
                case SDL_TEXTINPUT:
                    if (event.text.text[0] == " "[0]){
                        game.jump();
                    }
                break;

            }
        }

        game_graphic[game.pillars->ID_bottom_pillar].x -= MovePerFrame;
        game_graphic[game.pillars->ID_top_pillar   ].x -= MovePerFrame;

        game.fall();

        if(game_graphic[game.flappybird.ID].y + 40 >= 1000)
            running = false;

        game_graphic.new_frame();

        timer = SDL_GetTicks() - timer;

        if(timer < frame_time)
            SDL_Delay(frame_time - timer);

	}

    top_pillar_height = (random_int() % 410) + 140;
    topdrect.h = top_pillar_height;
    topsrcrect.h = top_pillar_height;
    botdrect = {.x = screenwidth, .y = top_pillar_height + 300, .w = pillar_width, .h = screenheight - (top_pillar_height + 300)};
    botsrcrect.h = 1000 - (top_pillar_height + 300);

    game.pillars[1].ID_top_pillar    = game_graphic.add_object(pillar_rin, topsrcrect, topdrect);
    game.pillars[1].ID_bottom_pillar = game_graphic.add_object(pillar_rin, botsrcrect, botdrect);

}