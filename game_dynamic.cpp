extern visual game_graphic;
extern const size_t pillar_width;
extern const size_t screenwidth;
extern const size_t screenheight;
extern const size_t MovePerFrame;

#define BIRD_Y_POS game_graphic[flappybird.ID].y
#define BIRD_WIDTH game_graphic[flappybird.ID].w
#define BIRD_HEIGHT game_graphic[flappybird.ID].h
#define BIRD_X_POS game_graphic[flappybird.ID].x
#define PILLAR0_TOP_X_POS game_graphic[pillars->ID_top_pillar].x
#define PILLAR0_BOT_X_POS game_graphic[pillars->ID_bottom_pillar].x
#define PILLAR0_BOT_Y_POS game_graphic[pillars->ID_bottom_pillar].y
#define PILLAR0_TOP_HEIGHT game_graphic[pillars->ID_top_pillar].h
#define PILLAR0_BOT_HEIGHT game_graphic[pillars->ID_bottom_pillar].h
#define PILLAR1_TOP_X_POS game_graphic[pillars[1].ID_top_pillar].x
#define PILLAR1_BOT_X_POS game_graphic[pillars[1].ID_bottom_pillar].x
#define PILLAR1_BOT_Y_POS game_graphic[pillars[1].ID_bottom_pillar].y
#define PILLAR1_TOP_HEIGHT game_graphic[pillars[1].ID_top_pillar].h
#define PILLAR1_BOT_HEIGHT game_graphic[pillars[1].ID_bottom_pillar].h
#undef pow


bool game_dynamic::colition(){

    if(PILLAR0_TOP_X_POS <= BIRD_X_POS + BIRD_WIDTH && BIRD_X_POS <= PILLAR0_TOP_X_POS + pillar_width)
        if(PILLAR0_TOP_HEIGHT >= BIRD_Y_POS || BIRD_Y_POS + BIRD_HEIGHT >= PILLAR0_BOT_Y_POS)
            return false;

    if(PILLAR1_TOP_X_POS <= BIRD_X_POS + BIRD_WIDTH && BIRD_X_POS <= PILLAR1_TOP_X_POS + pillar_width)
        if(PILLAR1_TOP_HEIGHT >= BIRD_Y_POS || BIRD_Y_POS + BIRD_HEIGHT >= PILLAR1_BOT_Y_POS)
            return false;

    if(BIRD_Y_POS + BIRD_HEIGHT >= 1000)
        return false;

    return true;
}

void game_dynamic::move_pillars(){

    PILLAR0_TOP_X_POS -= MovePerFrame;
    PILLAR0_BOT_X_POS -= MovePerFrame;
    PILLAR1_TOP_X_POS -= MovePerFrame;
    PILLAR1_BOT_X_POS -= MovePerFrame;

    if (PILLAR0_TOP_X_POS < 1)
    {
        PILLAR0_TOP_HEIGHT = (random_int() % 410) + 130;
        PILLAR0_BOT_Y_POS = PILLAR0_TOP_HEIGHT + 300;
        PILLAR0_BOT_HEIGHT = screenheight - PILLAR0_BOT_Y_POS;
        PILLAR0_TOP_X_POS = screenwidth;
        PILLAR0_BOT_X_POS = screenwidth;
        return;
    }

    if (PILLAR1_TOP_X_POS < 1)
    {
        PILLAR1_TOP_HEIGHT = (random_int() % 410) + 130;
        PILLAR1_BOT_Y_POS = PILLAR1_TOP_HEIGHT + 300;
        PILLAR1_BOT_HEIGHT = screenheight - PILLAR1_BOT_Y_POS;
        PILLAR1_TOP_X_POS = screenwidth;
        PILLAR1_BOT_X_POS = screenwidth;
        return;
    }

}

inline void game_dynamic::jump()
{

    static Uint32 timer;

    if(SDL_GetTicks() - timer > 200){
        flappybird.x_fall = 0;
        timer = SDL_GetTicks();
    }
}

void game_dynamic::fall()
{
    BIRD_Y_POS += pow(flappybird.x_fall, 2)/20 - 13;

    if(flappybird.x_fall < 25)
        flappybird.x_fall++;
}

#define pow SDL_pow
#undef BIRD_Y_POS
#undef BIRD_WIDTH
#undef BIRD_HEIGHT
#undef BIRD_X_POS
#undef PILLAR0_TOP_X_POS
#undef PILLAR0_BOT_X_POS
#undef PILLAR0_BOT_Y_POS
#undef PILLAR0_TOP_HEIGHT
#undef PILLAR0_BOT_HEIGHT
#undef PILLAR1_X_POS
#undef PILLAR1_BOT_X_POS
#undef PILLAR1_BOT_Y_POS
#undef PILLAR1_TOP_HEIGHT
#undef PILLAR1_BOT_HEIGHT