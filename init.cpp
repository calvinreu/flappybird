#include <iostream>

using std::vector;
using std::string;


const SDL_Rect bird_drect   = {.x = 100, .y = 480, .w = 55, .h = 40};
const SDL_Rect bird_srcrect = {.x = 0, .y = 0, .w = 55, .h = 40};
const SDL_Rect pillar_srcrect = {.x = 0, .y = 0, .w = 60, .h = 700};

const size_t screenheight = 1000;
const size_t screenwidth  = 500 ;

//textureC of these objects
const size_t flappybird_rin = 0;
const size_t pillar_rin = 1;

const string flappybirdIMG = "./flappybird.png";
const string pillarIMG     = "./pillar.png";

extern visual game_graphic;
extern game_dynamic game;
extern SDL_Event event; 



void init()
{
    std::vector<string> image_paths;

    image_paths.push_back(flappybirdIMG);
    image_paths.push_back(pillarIMG)    ;

    SDL_Init(SDL_INIT_EVERYTHING);
    
    game_graphic.init(image_paths, screenheight, screenwidth, false, "flappy bird", false);

    game.flappybird.ID = game_graphic.add_object(flappybird_rin, bird_srcrect, bird_drect);

}