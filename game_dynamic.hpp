const size_t pillar_width = 60;

struct bird
{
    size_t x_fall;
    INTERNAL::ID ID;
};

struct pillar
{
    INTERNAL::ID ID_top_pillar;
    INTERNAL::ID ID_bottom_pillar;
};

struct game_dynamic
{

    pillar pillars[2];
    bird flappybird;

    game_dynamic(){}

    void move_pillars();
    void jump();
    void fall();
    bool colition();

};

#include "game_dynamic.cpp"
