# flappybird

required libraries are
 - [SDL](https://www.libsdl.org/) oldest version 2.0
 - [SDL_image](https://www.libsdl.org/projects/SDL_image/) oldest version 2.0
 - [Graphic module](https://gitlab.com/calvinreu/modules/blob/master/window.hpp "window.hpp")
 - [Number generator](https://gitlab.com/calvinreu/modules/blob/master/random.hpp "random.hpp")